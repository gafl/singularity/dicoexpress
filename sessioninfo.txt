sessionInfo()
R version 3.6.1 (2019-07-05)
Platform: x86_64-conda_cos6-linux-gnu (64-bit)
Running under: Debian GNU/Linux 9 (stretch)

Matrix products: default
BLAS/LAPACK: /opt/miniconda/lib/R/lib/libRblas.so

locale:
[1] C

attached base packages:
[1] parallel  stats4    stats     graphics  grDevices utils     datasets 
[8] methods   base     

other attached packages:
 [1] coseq_1.10.0                SummarizedExperiment_1.16.0
 [3] DelayedArray_0.12.0         BiocParallel_1.20.0        
 [5] matrixStats_0.55.0          Biobase_2.46.0             
 [7] GenomicRanges_1.38.0        GenomeInfoDb_1.22.0        
 [9] IRanges_2.20.0              S4Vectors_0.24.0           
[11] BiocGenerics_0.32.0         edgeR_3.28.0               
[13] limma_3.42.0                data.table_1.12.2          
[15] RColorBrewer_1.1-2          dplyr_0.8.0.1              
[17] plyr_1.8.4                  ggpubr_0.2.4               
[19] magrittr_1.5                reshape2_1.4.3             
[21] gplots_3.0.1.1              FactoMineR_2.1             
[23] ggplot2_3.1.1              

loaded via a namespace (and not attached):
 [1] bitops_1.0-6           bit64_0.9-7            tensorA_0.36.1        
 [4] tools_3.6.1            backports_1.1.5        R6_2.4.0              
 [7] rpart_4.1-15           KernSmooth_2.23-15     Hmisc_4.3-0           
[10] DBI_1.1.0              lazyeval_0.2.2         colorspace_1.4-1      
[13] nnet_7.3-12            withr_2.1.2            tidyselect_0.2.5      
[16] gridExtra_2.3          DESeq2_1.26.0          bayesm_3.1-4          
[19] Rmixmod_2.1.2.2        bit_1.1-15.1           compiler_3.6.1        
[22] compositions_1.40-3    htmlTable_1.13.3       flashClust_1.01-2     
[25] caTools_1.17.1.2       scales_1.0.0           checkmate_1.9.4       
[28] mvtnorm_1.0-12         DEoptimR_1.0-8         robustbase_0.93-5     
[31] genefilter_1.68.0      DESeq_1.38.0           stringr_1.4.0         
[34] digest_0.6.18          foreign_0.8-71         XVector_0.26.0        
[37] base64enc_0.1-3        jpeg_0.1-8.1           pkgconfig_2.0.2       
[40] htmltools_0.3.6        plotrix_3.7-7          htmlwidgets_1.5.1     
[43] rlang_0.3.4            rstudioapi_0.10        RSQLite_2.1.1         
[46] gtools_3.8.1           HTSCluster_2.0.8       acepack_1.4.1         
[49] RCurl_1.98-1.1         GenomeInfoDbData_1.2.2 Formula_1.2-3         
[52] leaps_3.1              Matrix_1.2-17          Rcpp_1.0.1            
[55] munsell_0.5.0          scatterplot3d_0.3-41   stringi_1.4.3         
[58] MASS_7.3-51.3          zlibbioc_1.32.0        blob_1.1.1            
[61] grid_3.6.1             gdata_2.18.0           ggrepel_0.8.1         
[64] crayon_1.3.4           lattice_0.20-38        splines_3.6.1         
[67] annotate_1.64.0        HTSFilter_1.26.0       capushe_1.1.1         
[70] locfit_1.5-9.1         knitr_1.22             pillar_1.3.1          
[73] ggsignif_0.6.0         geneplotter_1.64.0     XML_3.98-1.19         
[76] glue_1.3.1             latticeExtra_0.6-29    png_0.1-7             
[79] gtable_0.3.0           purrr_0.3.3            assertthat_0.2.1      
[82] xfun_0.6               xtable_1.8-4           e1071_1.7-3           
[85] class_7.3-15           survival_2.44-1.1      tibble_2.1.1          
[88] memoise_1.1.0          AnnotationDbi_1.48.0   corrplot_0.84         
[91] cluster_2.0.8         

